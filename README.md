## 프로젝트에 사용한 YOLO v4 사용법

다음은 YOLO v4 의 개발자 겸 원작자의 [github](https://github.com/AlexeyAB/darknet) 에서 가져온 자료입니다.

![image](/uploads/cb24ba7d80105206d7b7e84654652435/image.png)

저희가 프로젝트에 사용한 기본 YOLO v4 세팅은 cocodataset 기반으로 이미 작성된 것입니다.

![image](/uploads/96910c6914131b14336686997dbb193d/image.png)

darknet-master 의 cfg 폴더 내에 yolov4.cfg 파일을 사용하겠습니다.

![image](/uploads/da0f73c6ceb7c6d32e44d92ea92b8b3b/image.png)

저희에게 중요한 설정값들은 \[net\] 의 batch, subdivisions, width, height, learning_rate, burn_in, max_batches, policy, steps 입니다.

모든 설정 값의 조절은 개발자가 알려준 방식을 따릅니다.

(1) batch 와 subdivision 은 컴퓨터의 성능에 따라 적당히 조절합니다. batch 가 크면 많은 양의 학습을, subdivision 이 작으면 더 세밀하게 학습을 할 수 있습니다. 하지만 subdivision 16에서 memory error 가 발생한다면 이보다 낮은 값 8이 권장됩니다.

(2) width 와 height 는 학습 시 사용할 이미지를 pixel 로 쪼갤 때 가로, 세로 몇 pixel 로 쪼갤 지 결정합니다. 보통 32의 배수를 사용합니다. width 와 height 값이 클 수록 한 이미지를 잘게 쪼갭니다. 이는 더 세밀한 학습을 할 수 있게 하여 더 작은 물체를 탐지할 수 있도록 합니다. 하지만 성능이 좋아진 만큼 학습 모델의 크기는 더 커지게 되고 영상의 FPS 가 낮아지거나 이미지의 처리 속도가 낮아질 수 있습니다. 위쪽의 차트를 확인해주시기 바랍니다.

(3) learning_rate 값은 기본적으로 0.0013, burn_in 은 1000 입니다. 하지만 사용자가 여러개의 GPU 를 사용한다면 값을 조절해도 좋습니다. learning_rate 는 GPU 가 N 개 일 경우에 Default 값 0.0013 을 N 으로 나눈 값, burn_in 은 Default 값 1000 에 N을 곱한 값을 사용합니다.

![image](/uploads/9638b64c3ece771bee09ae03bc73ffed/image.png)

앞서 말씀드린 것 처럼 기본 설정은 80개의 객체를 분류하는 cocodataset 을 기반으로 설정되어 있습니다. 저희가 하는 차량 계수 모델은 Bus, Car Truck 이렇게 세가지의 클래스만 사용합니다. 따라서 classes 부분을 수정합니다.

또한 \[yolo\] 위 \[convolutional\] 의 filters 값을 수정해야 합니다. classes 의 수에 맞게 최종 출력되는 부분이기에 classes 값을 기반으로 수정합니다. 주로 3 * \[5 + classes\] 의 값을 사용합니다. 저희의 classes 값은 3 이므로 filters 의 값은 24가 됩니다. 총 3개의 \[yolo\] 가 있으니 3개 모두 변경해 주시면 됩니다.

이제 darknet-master\data 폴더로 가셔서 .names, .data 파일을 생성해야 합니다. 메모장을 사용하셔 생성 후 확장자명을 변경해주시면 됩니다.

![image](/uploads/3dde9c69cebcdd3ba9129c26425e0585/image.png)

(1) .names

custom.names 라는 이름으로 생성했습니다. 파일명은 중요하지 않습니다. 이제 내용에 사용하실 라벨\(label\) 명을 순차적으로 적어주시면 됩니다.

![image](/uploads/5a7dec40fdc7ff9ceacda6204dcdc39d/image.png)

(2) .data

custom.data 라는 이름으로 생성했습니다. 위에서 말씀드린 것 처럼 이름은 중요하지 않습니다. 

![image](/uploads/d2e033d08abc0cc1ccbb5b198341aaaa/image.png)

classes = 3 (class 의 개수를 입력)

train = ./custom/train.txt (darknet-master 폴더 안에 data 이름의 폴더를 생성 후 그곳에 train.txt 가 있다고 알려주는 것입니다.)

valid 도 위와 같습니다.

train.txt 와 test.txt 생성법을 모르시는 분들을 위해 아래에 glob 파일 코드를 첨부하겠습니다.

names = 기존에 생성했던 .names 파일을 참고하여 라벨에 대한 이름을 인식하게 해줍니다.

backup = darknet-master 폴더 안에 backup 이름으로 폴더를 생성해주세요. 학습 후 나온 모델들을 저장할 폴더입니다.

##### glob 으로 train.txt 와 test.txt 생성하는 법

![image](/uploads/099075c28ebe7564737a204282b2f6dd/image.png)

경로 설정 시 가지고 계신 학습 데이터(이미지와 라벨)들이 모여있는 폴더의 경로를 입력해주시면 됩니다.

실행하시면 코드가 있는 경로 아래에 train.txt 와 test.txt 가 생성 될 것입니다.

마지막으로 pre-trained 된 모델을 다운 받겠습니다. 이 모델 또한 원작자의 github 에서 제공하는 파일입니다.

[모델 다운 받기](https://github.com/AlexeyAB/darknet/releases/download/darknet_yolo_v3_optimal/yolov4.conv.137)

마지막으로 학습에 곤련된 실행 명령어입니다.

이 또한 역시 원작자의 github 에서 가져왔습니다.

![image](/uploads/38cf219a3c9070a5775214386eedd90e/image.png)

COCO.data cfg/yolov4.cfg 는 학습 환경에 맞게 바꾸신 후 사용하시면 됩니다.